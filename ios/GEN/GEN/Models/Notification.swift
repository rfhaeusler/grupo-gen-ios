//
//  Notification.swift
//  GEN
//
//  Created by Luiz Aires Soares on 14/07/17.
//  Copyright © 2017 Jump Tecnologia. All rights reserved.
//

import UIKit
import SwiftyJSON

class GENNotification: BaseModel {
    
    var id: Int!
    var text: String!
    var title: String!
    var link: String!
    var bookID: String!
    var imageID: String!
    var areaID: String!
    var expirationDate: Date!
    
    override func toDictionary() -> [String : AnyObject] {
        var dict = super.toDictionary()
        dict["ID"] = self.id as AnyObject?
        dict["TEXTO"] = self.text as AnyObject?
        dict["TITULO"] = self.title as AnyObject?
        dict["LINK"] = self.link as AnyObject?
        dict["ID_LIVRO"] = self.bookID as AnyObject?
        dict["IMAGEM"] = self.imageID as AnyObject?
        dict["AREA"] = self.areaID as AnyObject?
        dict["DATA_VALIDADE"] = DateUtils.stringFromDate(self.expirationDate) as AnyObject?
        return dict
    }
    
    override class func fromJSON(_ objJSON: JSON) -> GENNotification {
        let entity = GENNotification()
        
        entity.id = Int(objJSON["ID"].stringValue)
        entity.text = objJSON["TEXTO"].string
        entity.title = objJSON["TITULO"].string
        entity.link = objJSON["LINK"].string
        entity.bookID = objJSON["ID_LIVRO"].string
        entity.imageID = objJSON["IMAGEM"].string
        entity.areaID = objJSON["AREA"].string
        entity.expirationDate = DateUtils.dateFromString(objJSON["DATA_VALIDADE"].string)
        
        return entity
    }

}
