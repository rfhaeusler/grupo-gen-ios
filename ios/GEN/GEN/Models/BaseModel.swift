//
//  BaseModel.swift
//  GEN
//
//  Created by Adheús Rangel on 9/14/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import SwiftyJSON

class BaseModel: NSObject {
    
    func toDictionary() -> [String : AnyObject] {
        return [String : AnyObject]()
    }
    
    func toJSON() -> JSON {
        return JSON(self.toDictionary())
    }
    
    class func fromJSON(_ objJSON: JSON) -> BaseModel {
        return BaseModel()
    }
    
    class func fromJSONArray(_ objJSON:JSON) -> [BaseModel]? {
        if let jsonArray = objJSON.array {
            return jsonArray.map { fromJSON($0) }
        }
        return nil
    }
}
