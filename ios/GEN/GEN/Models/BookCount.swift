//
//  BookCount.swift
//  GEN
//
//  Created by Luiz Aires Soares on 23/03/17.
//  Copyright © 2017 Jump Tecnologia. All rights reserved.
//

import UIKit
import SwiftyJSON

class BookCount: BaseModel {

    var total: Int!
    
    override func toDictionary() -> [String : AnyObject] {
        var dict = super.toDictionary()
        dict["totalLivros"] = self.total as AnyObject?
        return dict
    }
    
    override class func fromJSON(_ objJSON: JSON) -> BookCount {
        let entity = BookCount()
        entity.total = Int(objJSON["totalLivros"].stringValue)
        return entity
    }
}
