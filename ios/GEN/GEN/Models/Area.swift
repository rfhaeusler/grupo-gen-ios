//
//  Area.swift
//  GEN
//
//  Created by Adheús Rangel on 9/14/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import SwiftyJSON

class Area: BaseModel {
    
    var id: Int!
    var createdAt: Date!
    var title: String!
    var colorHex: String!
    var iconURL: String!
    var color: UIColor {
        get {
            return ColorUtils.colorFromHex(self.colorHex)
        }
    }
    
    override func toDictionary() -> [String : AnyObject] {
        var dict = super.toDictionary()
        dict["ID"] = self.id as AnyObject?
        dict["DATA_CADASTRO"] = DateUtils.stringFromDate(self.createdAt) as AnyObject?
        dict["TITULO"] = self.title as AnyObject?
        dict["COR"] = self.colorHex as AnyObject?
        dict["ICONE"] = self.iconURL as AnyObject?
        return dict
    }
    
    
    override class func fromJSON(_ objJSON:JSON) -> Area {
        let entity = Area()
        entity.id = Int(objJSON["ID"].stringValue)
        entity.createdAt = DateUtils.dateFromString(objJSON["DATA_CADASTRO"].string)
        entity.title = objJSON["TITULO"].string
        entity.colorHex = objJSON["COR"].string
        entity.iconURL = objJSON["ICONE"].string
        return entity
    }
}
