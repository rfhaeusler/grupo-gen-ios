//
//  Book.swift
//  GEN
//
//  Created by Adheús Rangel on 9/14/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import SwiftyJSON

class Book: BaseModel {
    
    var id: Int!
    var createdAt: Date!
    var title: String!
    
    var areaTitle: String!
    
    var categoryTitle: String!
    
    var edition: String!
    var format: String!
    var finalization: String!
    var editor: String!
    var author: String!
    var isbn: String!
    
    var weight: Double!
    
    var summary: String!
    
    var coverPictureURL: String!
    var previewPDFURL: String!
    
    var isRelease: Bool!
    
    var pageNumber: Int!
    
    var isSuplementar: Bool!
    
    var storeLink: String!
    
    override func toDictionary() -> [String : AnyObject] {
        var dict = super.toDictionary()
        
        dict["ID"] = self.id as AnyObject?
        dict["DATA_CADASTRO"] = DateUtils.stringFromDate(self.createdAt) as AnyObject?
        dict["TITULO"] = self.title as AnyObject?
        
        dict["AREA"] = self.areaTitle as AnyObject?
        
        dict["CATEGORIA"] = self.categoryTitle as AnyObject?
        
        dict["EDICAO"] = self.edition as AnyObject?
        dict["FORMATO"] = self.format as AnyObject?
        dict["ACABAMENTO"] = self.finalization as AnyObject?
        dict["EDITORA"] = self.editor as AnyObject?
        dict["AUTOR"] = self.author as AnyObject?
        dict["ISBN"] = self.isbn as AnyObject?
        
        dict["PESO"] = self.weight as AnyObject?
        
        dict["SINOPSE"] = self.summary as AnyObject?
        
        dict["CAPA"] = self.coverPictureURL as AnyObject?
        dict["AMOSTRA"] = self.previewPDFURL as AnyObject?
        
        dict["LANCAMENTO"] = self.isRelease as AnyObject?
        
        dict["PAGINAS"] = self.pageNumber as AnyObject?
        
        dict["COMPLEMENTAR"] = self.isSuplementar as AnyObject?
        
        dict["LINKLOJA"] = self.storeLink as AnyObject?
        
        return dict
    }
    
    
    override class func fromJSON(_ objJSON:JSON) -> Book {
        let entity = Book()
        
        entity.id = Int(objJSON["ID"].stringValue)
        entity.createdAt = DateUtils.dateFromString(objJSON["DATA_CADASTRO"].string)
        entity.title = objJSON["TITULO"].string
        
        entity.areaTitle = objJSON["AREA"].string
        
        entity.categoryTitle = objJSON["AREA"].string
        
        entity.edition = objJSON["EDICAO"].string
        entity.format = objJSON["FORMATO"].string
        entity.finalization = objJSON["ACABAMENTO"].string
        entity.editor = objJSON["EDITORA"].string
        entity.author = objJSON["AUTOR"].string
        entity.isbn = objJSON["ISBN"].string
        
        entity.weight = Double(objJSON["PESO"].stringValue)
        
//        entity.summary = objJSON["SINOPSE"].string?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        entity.summary = objJSON["SINOPSE"].string
        
        entity.coverPictureURL = objJSON["CAPA"].string
        entity.previewPDFURL = objJSON["AMOSTRA"].string
        
        entity.isRelease = objJSON["LANCAMENTO"].intValue == 1 ? true : false
        
        entity.pageNumber = objJSON["PAGINAS"].intValue
        
        entity.isSuplementar = objJSON["COMPLEMENTAR"].intValue == 1 ? true : false
        
        entity.storeLink = objJSON["LINKLOJA"].string
        
        return entity
    }
}

