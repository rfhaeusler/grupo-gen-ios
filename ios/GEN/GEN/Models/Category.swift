//
//  Category.swift
//  GEN
//
//  Created by Adheús Rangel on 9/14/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import SwiftyJSON

class Category: BaseModel {
    
    var id: Int!
    var createdAt: Date!
    var title: String!
    var areaTitle: String!
    var areaColorHex: String!
    var areaIconURL: String!
    var bookCount: Int = 0
    
    override func toDictionary() -> [String : AnyObject] {
        var dict = super.toDictionary()
        dict["ID"] = self.id as AnyObject?
        dict["DATA_CADASTRO"] = DateUtils.stringFromDate(self.createdAt) as AnyObject?
        dict["TITULO"] = self.title as AnyObject?
        dict["NOME_AREA"] = self.areaTitle as AnyObject?
        dict["COR_AREA"] = self.areaColorHex as AnyObject?
        dict["ICONE_AREA"] = self.areaIconURL as AnyObject?
        return dict
    }
    
    override class func fromJSON(_ objJSON: JSON) -> Category {
        let entity = Category()
        
        entity.id = Int(objJSON["ID"].stringValue)
        entity.createdAt = DateUtils.dateFromString(objJSON["DATA_CADASTRO"].string)
        entity.title = objJSON["TITULO"].string
        entity.areaTitle = objJSON["NOME_AREA"].string
        entity.areaColorHex = objJSON["COR_AREA"].string
        entity.areaIconURL = objJSON["ICONE_AREA"].string
        
        return entity
    }
}
