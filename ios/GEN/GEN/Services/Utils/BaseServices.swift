//
//  BaseServices.swift
//  Domum
//
//  Created by Adheús Rangel on 30/03/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

//PRODUCTION API ADDRESS
let API_BASE_ADDRESS = "http://appcatnov.grupogen.com.br/api/v1/"

//DEVELOPMENT API ADDRESS
//let API_BASE_ADDRESS = "http://www.grupoeh.com.br/grupogen/api/v1/"

class BaseServices {
    
    static var accessToken: String? = nil
    
    static var manager: Alamofire.SessionManager = BaseServices.createManager()
    
    class func APIRequest(method: HTTPMethod, apiPath: String, parameters: [String : AnyObject]?, callback: ((JSON?) -> Void)?) {
        
        let apiRequestAddress = API_BASE_ADDRESS + apiPath
        let customAllowedSet =  NSCharacterSet.urlQueryAllowed
        let formattedAPIRequestAddress = apiRequestAddress.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        
        var parameterEnconding: ParameterEncoding  = JSONEncoding.default
        if method == .get {
            parameterEnconding = URLEncoding.default
        }
        
        manager.request(formattedAPIRequestAddress!, method: method, parameters: parameters, encoding: parameterEnconding).responseJSON { (response) -> Void in
            print(response.debugDescription)
            if response.result.isSuccess || response.result.isFailure {
                if let value = response.result.value {
                    let serviceResponse = JSON(value)
                    if let callback = callback {
                        callback(serviceResponse)
                    }
                    return
                }
            }
            if let callback = callback {
                callback(nil)
            }
        }
    }
    
    // MARK: - Private Functions
    
    private class func getAccessToken() -> String? {
        if let sessionToken = BaseServices.accessToken {
            return sessionToken
        }
        let userDefaults = UserDefaults.standard
        if let accessToken:String = userDefaults.object(forKey: "access_token") as? String {
            BaseServices.accessToken = accessToken
            return accessToken
        }
        return nil
        
    }
    
    private class func request(method: HTTPMethod, URLString: URLConvertible,
                       parameters:[String : AnyObject]?,
                       encoding: ParameterEncoding, callback: ((URLRequest?, HTTPURLResponse?, Data?, Error?) -> Void)?) {
        
        manager.request(URLString, method:method, parameters: parameters, encoding: encoding).response { (defaultDataResponse) in
            if let callback = callback {
                callback(defaultDataResponse.request, defaultDataResponse.response, defaultDataResponse.data, defaultDataResponse.error)
            }
        }
    }
    
    private class func createManager() -> Alamofire.SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        if let _ = BaseServices.getAccessToken() {
            //configuration.HTTPAdditionalHeaders!["Authorization"] = "Bearer Granber " + accessToken
        }
        
        return Alamofire.SessionManager(
            configuration: configuration
        )
    }
    
    private class func resetManager() {
        accessToken = nil
        BaseServices.manager = createManager()
    }
    
    private class func APIImageUpload(apiPath:String, parameters:[String : AnyObject]?, imageData:NSData?, callback: ((ServiceResponse?) -> Void)?) {
        ImageUpload(apiPath: apiPath, parameters: parameters, imageData: imageData) { (data) -> Void in
            if data != nil {
                let serviceResponse = ServiceResponse.fromJSON(data!)
                if let callback = callback {
                    callback(serviceResponse)
                }
                return
            }
            if let callback = callback {
                callback(nil)
            }
        }
    }
    
    private class func APIRequestData(method: HTTPMethod, apiPath: String, parameters: [String : AnyObject]?, callback: ((Data?) -> Void)?) {
        let apiRequestAddress = API_BASE_ADDRESS + apiPath;
        manager.request(apiRequestAddress, method:method, parameters: parameters, encoding: JSONEncoding.default).responseData { (dataResponse) in
            if let callback = callback {
                callback(dataResponse.data)
                return
            }
        }
    }
    
    private class func ImageUpload(apiPath:String, parameters:[String : AnyObject]?, imageData:NSData?, callback: ((JSON?) -> Void)?) {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(url: NSURL(string: API_BASE_ADDRESS + apiPath)! as URL)
        mutableURLRequest.httpMethod = HTTPMethod.post.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        
        if let imageData = imageData {
            // add image
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"file\"; filename=\"file.png\"\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append(imageData as Data)
        }
        
        // add parameters
        if let parameters = parameters {
            print(parameters.description)
            for (key, value) in parameters {
                uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
            }
        }
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        
        
        // return URLRequestConvertible and NSData
        manager.upload(uploadData as Data, to: mutableURLRequest as! URLConvertible).responseJSON { (response) in
            if response.result.isSuccess || response.result.isFailure {
                
                if let value = response.result.value {
                    let jsonValue = JSON(value)
                    if let callback = callback {
                        callback(jsonValue)
                    }
                    return
                }
            }
            if let callback = callback {
                callback(nil)
            }
        }
    }
    
    private class func APIRequest(method: HTTPMethod, apiPath: String, data: JSON, callback: ((JSON?) -> Void)?) {
        let urlRequest = NSMutableURLRequest(url: NSURL(string: API_BASE_ADDRESS + apiPath)! as URL)
        
        var methodString = "GET"
        switch(method) {
        case .post:
            methodString =  "POST"
            break
        case .put:
            methodString =  "PUT"
            break
        case .delete:
            methodString =  "DELETE"
            break
            
        default: break
        }
        
        urlRequest.httpMethod = methodString
        let body = data.description.data(using: String.Encoding.utf8)
        urlRequest.httpBody = body
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(body!.count.description, forHTTPHeaderField: "Content-Length")
        manager.request(urlRequest as! URLRequestConvertible).responseJSON { (response) -> Void in
            print(response.debugDescription)
            if response.result.isSuccess || response.result.isFailure {
                if let value = response.result.value {
                    print(JSON(value))
                    if let callback = callback {
                        callback(JSON(value))
                    }
                    return
                }
            }
            if let callback = callback {
                callback(nil)
            }
        }
    }
    
    private class func APIRequestListing(method: HTTPMethod, apiPath: String, data: JSON, callback: ((JSON?) -> Void)?) {
        let urlRequest = NSMutableURLRequest(url: NSURL(string: API_BASE_ADDRESS + apiPath)! as URL)
        
        var methodString = "GET"
        switch(method) {
        case .post:
            methodString =  "POST"
            break
        case .put:
            methodString =  "PUT"
            break
        case .delete:
            methodString =  "DELETE"
            break
            
        default: break
        }
        urlRequest.httpMethod = methodString
        let body = data.description.data(using: String.Encoding.utf8)
        urlRequest.httpBody = body
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(body!.count.description, forHTTPHeaderField: "Content-Length")
        
        manager.request(urlRequest as! URLRequestConvertible).responseJSON { (response) -> Void in
            if response.result.isSuccess || response.result.isFailure {
                print("Request did return")
                if let value = response.result.value {
                    print(JSON(value))
                    if let callback = callback {
                        callback(JSON(value))
                    }
                    return
                }
            }
            if let callback = callback {
                callback(nil)
            }
        }
        
        
    }
    
    private class func Request(method: HTTPMethod, url: String, parameters: [String : AnyObject]?, callback: ((JSON?) -> Void)?) {
        manager.request(url, method:method, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) -> Void in
            if response.result.isSuccess || response.result.isFailure {
                
                if let value = response.result.value {
                    let jsonValue = JSON(value)
                    if let callback = callback {
                        callback(jsonValue)
                    }
                    return
                }
            }
            if let callback = callback {
                callback(nil)
            }
        }
    }
}
