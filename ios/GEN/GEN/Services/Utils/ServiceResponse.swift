//
//  ServiceResponse.swift
//  Domum
//
//  Created by Adheús Rangel on 6/26/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import SwiftyJSON

class ServiceResponse {
    var success:Bool! = false
    var code:Int = 0
    var message:String? = nil
    var data:NSObject? = nil
    var jsonData:JSON? = nil
    
    class func fromJSON(_ json:JSON) -> ServiceResponse {
        let response = ServiceResponse()
        response.success = json["success"].bool
        response.message = json["message"].string
        response.jsonData = json
        return response
        
    }
}
