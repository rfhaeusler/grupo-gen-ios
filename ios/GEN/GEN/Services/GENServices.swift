//
//  UnimedServices.swift
//  Unimed On Air
//
//  Created by Adheús Rangel on 1/25/16.
//  Copyright © 2016 Infinity Labs. All rights reserved.
//

import Foundation
import SwiftyJSON

class GENServices: BaseServices {

    class func listAreas(_ callback: @escaping ([Area]?) -> Void) {
        APIRequest(method: .get, apiPath: "areas", parameters: nil) { (response) -> Void in
            if let jsonData = response {
                let areas = Area.fromJSONArray(jsonData) as? [Area]
                callback(areas)
                return
            }
            callback(nil)
        }
    }
    
    class func listCategories(_ areaID: Int, callback: @escaping ([Category]?) -> Void) {
        APIRequest(method: .get, apiPath: String(format: "categorias/%d", areaID), parameters: nil) { (response) -> Void in
            if let jsonData = response {
                let categories = Category.fromJSONArray(jsonData) as? [Category]
                callback(categories)
                return
            }
            callback(nil)
        }
    }
    
    class func categoriesCount(_ areaID: Int, categoryID: Int, callback: @escaping (BookCount?) -> Void) {
        APIRequest(method: .get, apiPath: String(format: "countlivros/\(areaID)/\(categoryID)"), parameters: nil) { (response) -> Void in
            if let jsonData = response, let bookCount = BookCount.fromJSONArray(jsonData) as? [BookCount] {
                callback(bookCount[0])
                return
            }
            callback(nil)
        }
    }
    
    class func listBooks(_ areaID: Int, categoryID: Int, callback: @escaping ([Book]?) -> Void) {
        var path: String = String(format: "livros/%d", areaID)
        if categoryID != -1 {
            path.append(String(format:"/%d", categoryID))
        }
        APIRequest(method: .get, apiPath: path, parameters: nil) { (response) -> Void in
            if let jsonData = response {
                let books = Book.fromJSONArray(jsonData) as? [Book]
                callback(books)
                return
            }
            callback(nil)
        }
    }
    
    class func searchBooks(_ terms: String!, callback: @escaping ([Book]?) -> Void) {
        var path: String = String(format: "livros/%@", terms)
        let currentAreas = AppCache.getAreas()
        if currentAreas.count > 0 {
            var areasPath: String = "/("
            for areaID in currentAreas {
                areasPath.append(String(format: "%d,", areaID))
            }
            areasPath.remove(at: areasPath.index(before: areasPath.endIndex))
            areasPath.append(")")
            path.append(areasPath)
        }
        APIRequest(method: .get, apiPath: path, parameters: nil) { (response) -> Void in
            if let jsonData = response {
                let books = Book.fromJSONArray(jsonData) as? [Book]
                callback(books)
                return
            }
            callback(nil)
        }
    }
    
    class func notifications(_ callback: @escaping ([GENNotification]?) -> Void) {
        APIRequest(method: .get, apiPath: "notificacoes", parameters: nil) { (response) -> Void in
            if let jsonData = response {
                if let notifications = GENNotification.fromJSONArray(jsonData) as? [GENNotification] {
                    let selectedAreas = AppCache.getAreas()
                    var filteredNotifications: [GENNotification] = notifications.filter { selectedAreas.contains(Int($0.areaID)!) }
                    filteredNotifications = filteredNotifications.filter { $0.expirationDate > Date() }
                    callback(filteredNotifications)
                    return
                }
            }
            callback(nil)
        }
    }

}
