//
//  AppDelegate.swift
//  GEN
//
//  Created by Adheús Rangel on 8/22/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import CoreData
import KYDrawerController
import Fabric
import Crashlytics
import UserNotifications

enum SideMenuState {
    case open, closed
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // change the back button, using default tint color
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = ColorUtils.getAccentColor()
        UINavigationBar.appearance().backIndicatorImage = UIImage(named:"BackIcon")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named:"BackIcon")
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: UIControlState())
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: .selected)
        
        
        
        let sharedApplication = UIApplication.shared
        sharedApplication.statusBarStyle = UIStatusBarStyle.lightContent
        sharedApplication.applicationIconBadgeNumber = 0
        
        if let drawerController = AppDelegate.getDrawerController() {
            drawerController.drawerDirection = .left
        }
        
        Fabric.with([Crashlytics.self])
        
        self.requestUserForNotifications()
        
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
//        } else {
//            UIApplication.shared.cancelAllLocalNotifications()
//        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - SideMenu
    
    class func getDrawerController() -> KYDrawerController? {
        if let currentWindow  = UIApplication.shared.delegate?.window {
            return currentWindow?.rootViewController as? KYDrawerController
        }
        return nil
    }
    
    class func setSideMenuState(_ state:SideMenuState, animated:Bool) {
        var drawerState = KYDrawerController.DrawerState.opened
        if state == SideMenuState.closed {
            drawerState = KYDrawerController.DrawerState.closed
        }
        if let drawerViewController = getDrawerController() {
            drawerViewController.setDrawerState(drawerState, animated: animated)
            if let menuViewControler = drawerViewController.drawerViewController as? MenuViewController {
                menuViewControler.configureView()
            }
        }
    }
    
    class func navigationViewController() -> UINavigationController? {
        if let navigationController = AppDelegate.getDrawerController()!.mainViewController as? UINavigationController {
            return navigationController
        }
        return nil
    }
    
    class func currentMainViewController() -> UIViewController? {
        if let navigationController = navigationViewController() {
            return navigationController.viewControllers[0]
        }
        return nil
    }
    
    class func navigateToViewController(_ viewController:UIViewController) {
        if let navigationController = navigationViewController() {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
    // MARK: - Notifications
    
    func requestUserForNotifications() {
        if #available(iOS 10.0, *) {
            let options: UNAuthorizationOptions = [.alert, .sound, .badge]
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
                if settings.authorizationStatus != .authorized {
                    UNUserNotificationCenter.current().requestAuthorization(options: options, completionHandler: { (authorization, error) in
                        UNUserNotificationCenter.current().delegate = self
                    })
                } else {
                    UNUserNotificationCenter.current().delegate = self
                }
            })
        } else {
            // Fallback on earlier versions
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.handleUNNotification(notification: response.notification.request.content)
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        self.handleLocalNotification(notification: notification)
    }
    
    func createAndShowNotification(notification: GENNotification) {
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = notification.title
            
            if let text = notification.text, text != "" {
                content.body = text
            } else {
                content.title = ""
                content.body = notification.title
            }
            
            if let bookID = notification.bookID, let areaID = notification.areaID {
                content.userInfo = ["book_id" : bookID, "area_id" : areaID]
            }
            if let link = notification.link {
                content.userInfo["link"] = link
            }
            content.sound = UNNotificationSound.default()
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: Double(notification.id), repeats: false)
            
            let notificationIdentifier = String(format: "gen.notification.%d", notification.id)
            
            let request = UNNotificationRequest(identifier: notificationIdentifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                if let e = error {
                    print(e)
                } else {
                    UIApplication.shared.applicationIconBadgeNumber += 1
                }
            })
        } else {
            // Fallback on earlier versions
            let not = UILocalNotification()
            not.alertTitle = notification.title
            if let body = notification.text {
                not.alertBody = body
            }
            if let bookID = notification.bookID, let areaID = notification.areaID {
                not.userInfo = ["book_id" : bookID, "area_id" : areaID]
            }
            if let link = notification.link {
                if (not.userInfo != nil) {
                    not.userInfo!["link"] = link
                } else {
                    not.userInfo = ["link" : link]
                }
            }
            
            not.fireDate = Date.init(timeIntervalSinceNow: 1.0)
            
            UIApplication.shared.scheduleLocalNotification(not)
        }
    }
    
    func handleLocalNotification(notification: UILocalNotification) {
        if let bookID = notification.userInfo?["book_id"] as? String, let areaID = notification.userInfo?["area_id"] as? String {
            GENServices.listBooks(Int(areaID)!, categoryID: -1, callback: { (books) in
                if let list = books {
                    let filteredBooks = list.filter { $0.id == Int(bookID) }
                    if filteredBooks.count > 0 {
                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let bookDetails: BookDetailsViewController = storyboard.instantiateViewController(withIdentifier:"BookDetailsViewController") as! BookDetailsViewController
                        bookDetails.book = filteredBooks.first!
                        AppDelegate.navigateToViewController(bookDetails)
                    }
                }
            })
            UIApplication.shared.applicationIconBadgeNumber -= 1
            return
        }
        if let link = notification.userInfo?["link"] as? String {
            let url: URL = URL(string: link)!
            UIApplication.shared.openURL(url)
            UIApplication.shared.applicationIconBadgeNumber -= 1
        }
    }
    
    @available(iOS 10.0, *)
    private func handleUNNotification(notification: UNNotificationContent) {
        if let bookID = notification.userInfo["book_id"] as? String, let areaID = notification.userInfo["area_id"] as? String {
            GENServices.listBooks(Int(areaID)!, categoryID: -1, callback: { (books) in
                if let list = books {
                    let filteredBooks = list.filter { $0.id == Int(bookID) }
                    if filteredBooks.count > 0 {
                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let bookDetails: BookDetailsViewController = storyboard.instantiateViewController(withIdentifier:"BookDetailsViewController") as! BookDetailsViewController
                        bookDetails.book = filteredBooks.first!
                        AppDelegate.navigateToViewController(bookDetails)
                    }
                }
            })
            UIApplication.shared.applicationIconBadgeNumber -= 1
            return
        }
        if let link = notification.userInfo["link"] as? String {
            let url: URL = URL(string: link)!
            UIApplication.shared.openURL(url)
            UIApplication.shared.applicationIconBadgeNumber -= 1
        }
    }


    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "br.com.jumptec.GEN" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "GEN", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

