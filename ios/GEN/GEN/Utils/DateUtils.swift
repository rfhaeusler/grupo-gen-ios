//
//  DateUtils.swift
//  Domum
//
//  Created by Adheús Rangel on 7/13/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation


class DateUtils {
    
    static let onlyTimeFormat = "HH:mm:ss"
    static let onlyDateFormat = "yyyy-MM-dd"
    static let dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let preciseDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    static let readableDateFormat = "dd/MM/yyyy"
    static var dateFormatter = DateFormatter()
    
    
    class func stringFromDate(_ date:Date?) -> String! {
        if let date = date {
            dateFormatter.dateFormat = dateFormat
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    class func stringFromDateOnly(_ date:Date) -> String {
        dateFormatter.dateFormat = onlyDateFormat
        return dateFormatter.string(from: date)
    }
    
    class func dateFromString(_ dateStringMaybe:String?) -> Date? {
        if let dateString = dateStringMaybe {
            if let date = tryFormatDateWith(dateString, dateFormat: dateFormat) {
                return date
            } else if let date = tryFormatDateWith(dateString, dateFormat: preciseDateFormat) {
                return date
            } else if let date = tryFormatDateWith(dateString, dateFormat: onlyDateFormat) {
                return date
            } else if let date = tryFormatDateWith(dateString, dateFormat: onlyTimeFormat) {
                return date
            }
        }
        return nil
    }
    
    fileprivate class func tryFormatDateWith(_ dateString:String, dateFormat:String) -> Date? {
        dateFormatter.dateFormat = dateFormat
        if let date = dateFormatter.date(from: dateString) {
            return date
        }
        return nil
    }
    
    
    class func stringTimeFromDate(_ date:Date) -> String {
        dateFormatter.dateFormat = nil
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .none
        return dateFormatter.string(from: date)
    }
    
    class func readableStringFromDate(_ date:Date) -> String {
        dateFormatter.dateFormat = nil
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .full
        return dateFormatter.string(from: date)
    }
    
    class func readableShortStringFromDate(_ date:Date) -> String {
        dateFormatter.dateFormat = nil
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .short
        return dateFormatter.string(from: date)
    }
    
    
    
    class func midnightDate(_ date:Date) -> Date {
        dateFormatter.dateFormat = onlyDateFormat
        let dateString = dateFormatter.string(from: date)
        return DateUtils.dateFromString(dateString + "T00:00:00Z")!
    }
    
}
