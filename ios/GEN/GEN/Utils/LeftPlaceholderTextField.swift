//
//  LeftPlaceHolderTextField.swift
//  Jump Park
//
//  Created by Adheús Rangel on 8/3/16.
//  Copyright © 2016 jump. All rights reserved.
//

import UIKit

class LeftPlaceholderTextField: UITextField {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    static let DEFAULT_PLACEHOLDER_SPACING:CGFloat = 108
    
    var placeholderSpacing:CGFloat = DEFAULT_PLACEHOLDER_SPACING
    
    fileprivate var leftPlaceholderText:String?
    
    var leftPlacehoderLabel:UILabel?
    
    func setLeftPlaceHolderView(_ placeholderView:UIView) {
        self.leftView = placeholderView
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    
    var leftPlaceholder:String? {
        get {
            return leftPlaceholderText
        }
        
        set {
            self.leftPlaceholderText = newValue
            self.updateLeftPlaceholder()
        }
    }
    
    fileprivate func updateLeftPlaceholder() {
        if let leftPlacehoderLabel = self.leftPlacehoderLabel {
            leftPlacehoderLabel.text = leftPlaceholderText
        } else {
            self.leftPlacehoderLabel = UILabel(frame: CGRect(x: 0,y: 0, width: self.placeholderSpacing, height: self.frame.size.height))
            self.leftPlacehoderLabel?.font = self.font
            self.leftPlacehoderLabel?.textColor = UIColor.lightGray
            self.leftPlacehoderLabel?.text = leftPlaceholderText
            
            self.setLeftPlaceHolderView(self.leftPlacehoderLabel!)
        }
    }
}
