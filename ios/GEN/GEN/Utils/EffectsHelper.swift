//
//  EffectsHelper.swift
//  Granber
//
//  Created by Adheús Rangel on 3/10/16.
//  Copyright © 2016 Granber. All rights reserved.
//

import UIKit

class EffectsHelper {

    class func applyShadow(_ view:UIView) {
        view.layer.shadowOffset = CGSize(width: -1, height: 0)
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 2
    }
    
    
    class func applyBorders(_ view:UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = view.tintColor!.cgColor
    }
    
    
    class func circleView(_ view:UIView) {
        view.layer.masksToBounds = true
        view.layer.cornerRadius = view.frame.size.width / 2
    }
    
    class func roundEdges(_ view:UIView) {
        view.layer.masksToBounds = true
        var largerLength = view.frame.size.width
        if largerLength < view.frame.size.height {
            largerLength = view.frame.size.height
        }
        view.layer.cornerRadius = largerLength * 0.02
    }
    
    class func applyRoundBorders(_ view:UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = ColorUtils.getAccentColor().cgColor
        roundEdges(view)
        
    }
    
    class func paintImage(_ imageName:String, withColor:UIColor) -> UIImage? {
        if let image = UIImage(named: imageName) {
            return paintImage(image, withColor: withColor)
        }
        return nil
    }
    
    class func paintImage(_ image:UIImage, withColor:UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        withColor.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: image.size.height)
        context?.scaleBy(x: 1.0, y: -1.0);
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height) as CGRect
        context?.clip(to: rect, mask: image.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    class func loadImage(_ imageName:String, toButton:UIButton, withColor:UIColor) {
        let image = UIImage(named: imageName)!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        toButton.setImage(image, for: UIControlState())
        toButton.imageView!.tintColor = withColor
    }
    
    class func loadImage(_ imageName:String, toImageView:UIImageView, withColor:UIColor) {
        let image = UIImage(named: imageName)!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        toImageView.image = image
        toImageView.tintColor = withColor
    }
    
    class func applyBottomBorder(_ view:UIView, borderSize:CGFloat, color:UIColor) {
            let bottomBorder = CALayer()
            bottomBorder.name = "bottomBorder"
            bottomBorder.frame = CGRect(x: 0, y: view.frame.size.height - borderSize, width:  view.frame.size.width, height: view.frame.size.height)
            bottomBorder.borderColor =  color.cgColor
            bottomBorder.borderWidth = borderSize
            view.layer.addSublayer(bottomBorder)
            
            view.layer.masksToBounds =  true
    }
    
}
