//
//  AppCache.swift
//  GEN
//
//  Created by Adheús Rangel on 9/16/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

class AppCache {
    static let IS_NOT_FIRST_RUN_KEY = "IsNotFirstRun"
    static let CONFIG_AREAS_KEY = "ConfigAreas"
    static var test:Int?
    
    class func isFirstRun() -> Bool {
        let userDefaults = UserDefaults.standard
        let IsNotFirstRun = userDefaults.bool(forKey: IS_NOT_FIRST_RUN_KEY)
        if IsNotFirstRun == false {
            userDefaults.set(true, forKey: IS_NOT_FIRST_RUN_KEY)
            userDefaults.synchronize()
        }
        return !IsNotFirstRun
    }
    
    class func getAreas() -> [Int] {
        let userDefaults = UserDefaults.standard
        if let areas = userDefaults.array(forKey: CONFIG_AREAS_KEY) as? [Int] {
            return areas
        }
        
        return [Int]()
    }
    
    class func saveArea(area:Area) {
        var areasIds = getAreas()
        if !areasIds.contains(area.id) {
            areasIds.append(area.id)
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(areasIds, forKey: CONFIG_AREAS_KEY)
            userDefaults.synchronize()
        }
    }
    
    class func removeArea(area:Area) {
        var areasIds = getAreas()
        areasIds = areasIds.filter{ $0 != area.id }
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(areasIds, forKey: CONFIG_AREAS_KEY)
        userDefaults.synchronize()
    }
}
