//
//  SelectableItemTableViewCell.swift
//  Granber
//
//  Created by Adheús Rangel on 5/10/16.
//  Copyright © 2016 Granber. All rights reserved.
//

import UIKit

class SelectableItemTableViewCell: UITableViewCell {

    @IBOutlet weak var title:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setItem(_ object:AnyObject) {
        self.title.text = object.description
    }

}

