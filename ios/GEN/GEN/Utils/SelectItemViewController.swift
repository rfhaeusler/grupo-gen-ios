//
//  SelectItemViewController.swift
//  Granber
//
//  Created by Adheús Rangel on 5/10/16.
//  Copyright © 2016 Granber. All rights reserved.
//

import UIKit

class SelectItemViewController: CustomNavigationBarViewController {
    
    
    @IBOutlet weak var tableView:UITableView!
    
    var items:[NSObject]?
    
    var onItemSelectedListener:((NSObject) -> Void)?
    
    
    
    
    
    class func new<C: UIViewController>(_ title:String? = nil, fromViewController:C, items:[NSObject], onItemSelectedListener:@escaping (NSObject) -> Void) -> SelectItemViewController where C:UIPopoverPresentationControllerDelegate
    {
            let selectItemViewController = StoryboardLoader.loadViewController("SelectItemViewController") as! SelectItemViewController
        selectItemViewController.items = items;
        selectItemViewController.onItemSelectedListener = onItemSelectedListener
        
        if let title = title {
            selectItemViewController.title = title
        }
        
        let nav = UINavigationController(rootViewController: selectItemViewController)
        nav.modalPresentationStyle = .popover
        let popover = nav.popoverPresentationController
        let width:CGFloat = 240
        var height:CGFloat = (44.0 * CGFloat(items.count))
        if(height > 320) {
            height = 320
        }
        selectItemViewController.preferredContentSize = CGSize(width: width, height: height)
        if let popover = popover {
            popover.delegate = fromViewController
            popover.sourceView = fromViewController.view
            popover.sourceRect = CGRect(x: (fromViewController.view.frame.width - width)/2,y: (fromViewController.view.frame.height - height)/2, width: width, height: height)
            popover.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }
        
        fromViewController.present(nav, animated: true, completion: nil)
        
        return selectItemViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK - UITableview delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let items = self.items {
            return items.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let selectedItem = self.items?[(indexPath as NSIndexPath).row] {
            if let onItemSelectedListener = self.onItemSelectedListener {
                onItemSelectedListener(selectedItem)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let myCell = tableView.dequeueReusableCell(withIdentifier: "SelectableItemTableViewCell", for: indexPath) as! SelectableItemTableViewCell
        
        myCell.setItem(self.items![(indexPath as NSIndexPath).row])
        
        return myCell
    }

}
