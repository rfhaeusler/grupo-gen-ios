# TutorialScrollViewController

[![CI Status](http://img.shields.io/travis/Chenglu Li/TutorialScrollViewController.svg?style=flat)](https://travis-ci.org/Chenglu Li/TutorialScrollViewController)
[![Version](https://img.shields.io/cocoapods/v/TutorialScrollViewController.svg?style=flat)](http://cocoapods.org/pods/TutorialScrollViewController)
[![License](https://img.shields.io/cocoapods/l/TutorialScrollViewController.svg?style=flat)](http://cocoapods.org/pods/TutorialScrollViewController)
[![Platform](https://img.shields.io/cocoapods/p/TutorialScrollViewController.svg?style=flat)](http://cocoapods.org/pods/TutorialScrollViewController)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TutorialScrollViewController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "TutorialScrollViewController"
```

## Author

Chenglu Li, kabelee92@gmail.com

## License

TutorialScrollViewController is available under the MIT license. See the LICENSE file for more info.
