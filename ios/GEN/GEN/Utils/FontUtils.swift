//
//  FontUtils.swift
//  Jump Park
//
//  Created by Adheús Rangel on 6/13/16.
//  Copyright © 2016 jump. All rights reserved.
//

import Foundation
import UIKit

class FontUtils {
    
    
    static let defaultFontNames = ["normal":"MyriadApple-Text", "medium":"MyriadApple-Medium", "semibold":"MyriadApple-Semibold", "bold":"MyriadApple-Bold"]
    
    //For debugging purposes
    class func printFontNamesList() {
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
    
    fileprivate class func getFont(_ style:String, size:CGFloat) -> UIFont {
        return UIFont(name: defaultFontNames[style]!, size: size)!
    }
    
    
    class func getBoldFont(_ size:CGFloat) -> UIFont {
        return getFont("bold", size:size)
    }
    
    class func getSemiboldFont(_ size:CGFloat) -> UIFont {
        return getFont("semibold", size:size)
    }
    
    class func getMediumFont(_ size:CGFloat) -> UIFont {
        return getFont("medium", size:size)
    }
    
    class func getNormalFont(_ size:CGFloat) -> UIFont {
        return getFont("normal", size:size)
    }
    
    
    
}
