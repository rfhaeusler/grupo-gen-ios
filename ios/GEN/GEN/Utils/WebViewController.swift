//
//  WebViewController.swift
//  Unimed On Air
//
//  Created by Adheús Rangel on 2/4/16.
//  Copyright © 2016 Infinity Labs. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webview:UIWebView!
    
    var address:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let address = self.address {
            ProgressHUD.show(self.view)
            self.webview.loadRequest(URLRequest(url: URL(string: address)!))
        }
        self.navigationController!.navigationBar.barStyle = UIBarStyle.blackTranslucent
        self.navigationController!.navigationBar.barTintColor = ColorUtils.getTitleBarBaseColor()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ProgressHUD.hide(self.view)
    }

}
