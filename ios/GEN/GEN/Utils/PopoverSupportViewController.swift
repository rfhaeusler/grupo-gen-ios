//
//  PopoverSupportViewController.swift
//  Jump Park
//
//  Created by Adheús Rangel on 7/1/16.
//  Copyright © 2016 jump. All rights reserved.
//

import UIKit

class PopoverSupportViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    // MARK - UIPopoverPresentation delegate
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
