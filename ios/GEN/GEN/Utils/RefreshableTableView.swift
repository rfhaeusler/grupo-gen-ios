//
//  RefreshableTableView.swift
//  Domum
//
//  Created by Adheús Rangel on 9/15/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import UIKit

class RefreshableTableView : UITableView {
    
    var mRefreshControl:UIRefreshControl? = nil
    
    var refreshAction:(() -> Void)? = nil
    
    func addRefreshViewAction(_ action:@escaping ()->Void) -> Void {
        self.mRefreshControl = UIRefreshControl()
        self.mRefreshControl?.backgroundColor = ColorUtils.getHUDBackgroundColor()
        self.mRefreshControl?.tintColor = ColorUtils.getHUDForegroundColor()
        self.mRefreshControl?.addTarget(self, action: #selector(RefreshableTableView.callRefreshAction), for: .valueChanged)
        self.refreshAction = action
        self.addSubview(self.mRefreshControl!)
    }
    
    
    func callRefreshAction() {
        if let refreshAction = self.refreshAction {
            refreshAction()
        }
    }
    
    func stopRefreshing() {
        self.mRefreshControl?.endRefreshing()
    }
}
