//
//  HUDProgress.swift
//  Domum
//
//  Created by Adheús Rangel on 9/11/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import SVProgressHUD

class ProgressHUD {
    
    
    class func show(_ view:UIView?, message:String? = nil, color:UIColor = ColorUtils.getHUDForegroundColor()) {
        if let view = view {
            view.isUserInteractionEnabled = false
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        SVProgressHUD.setForegroundColor(color)
        SVProgressHUD.setBackgroundColor(ColorUtils.getHUDBackgroundColor())
        if let message = message {
            SVProgressHUD.show(withStatus: message)
        } else {
            SVProgressHUD.show()
        }
    }
    
    class func hide(_ view:UIView?) {
        if let view = view {
            view.isUserInteractionEnabled = true
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        SVProgressHUD.dismiss()
    }
    
    class func showProgressOnStatusBar() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    class func hideProgressOnStatusBar() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
