//
//  FormUtils.swift
//  Domum
//
//  Created by Adheús Rangel on 8/18/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import UIKit

class FormUtils {
    
    class func containsWhitescapes(_ text:String?) -> Bool {
        if let text = text {
            return text.contains(" ")
        }
        return false
    }
    
    class func length(_ text:String?) -> Int {
        if let text = text {
            return text.characters.count
        }
        return 0
    }
    
    class func isBlank(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespaces)
        return trimmed.isEmpty
    }
    
    class func isBlank(_ text: String?) -> Bool {
        if let text = text {
            return isBlank(text)
        }
        return true
    }
}
