//
//  ColorUtils.swift
//  Domum
//
//  Created by Adheús Rangel on 6/30/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import UIKit

class ColorUtils {
    
    //MARK - COLORS
    
    class func getTitleBarBaseColor() -> UIColor {
        return getAccentColor()
    }
    
    class func getTitleBarTintColor() -> UIColor {
        return UIColor.white
    }
    
    class func getAccentColor() -> UIColor {
        return ColorUtils.colorFromHex("#2B2B2B")
    }
    
    class func getSubAccentColor() -> UIColor {
        return UIColor.black
        
    }
    
    
    
    class func getDefaultColor() -> UIColor {
        return UIColor(red:0.36, green:0.36, blue:0.36, alpha:1.0)
    }
    
    class func getLighterDefaultColor() -> UIColor {
        return UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0)
    }
    
    
    
    class func getSuccessColor() -> UIColor {
        return UIColor(red:0.26, green:0.62, blue:0.30, alpha:1.0)
    }
    class func getWarningColor() -> UIColor {
        return UIColor(red:1.00, green:0.76, blue:0.25, alpha:1.0)
    }
    
    class func getErrorColor() -> UIColor {
        return UIColor(red:0.62, green:0.00, blue:0.10, alpha:1.0)
    }
    
    
    class func getHUDBackgroundColor() -> UIColor {
        return UIColor(red:0.973, green:0.973, blue:0.973, alpha:0.9)
    }
    class func getHUDForegroundColor() -> UIColor {
        return ColorUtils.getAccentColor()
    }
    
    //MARK - COLORS UTILS
    
    class func colorFromHex(_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines as NSCharacterSet) as CharacterSet).uppercased()
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    
    class func darkerColorForColor(_ color: UIColor) -> UIColor {
        
        var r:CGFloat = 0, g:CGFloat = 0, b:CGFloat = 0, a:CGFloat = 0
        
        if color.getRed(&r, green: &g, blue: &b, alpha: &a){
            return UIColor(red: max(r - 0.2, 0.0), green: max(g - 0.2, 0.0), blue: max(b - 0.2, 0.0), alpha: a)
        }
        
        return UIColor()
    }
    
    class func lighterColorForColor(_ color: UIColor) -> UIColor {
        
        var r:CGFloat = 0, g:CGFloat = 0, b:CGFloat = 0, a:CGFloat = 0
        
        if color.getRed(&r, green: &g, blue: &b, alpha: &a){
            return UIColor(red: min(r + 0.2, 1.0), green: min(g + 0.2, 1.0), blue: min(b + 0.2, 1.0), alpha: a)
        }
        
        return UIColor()
    }
}
