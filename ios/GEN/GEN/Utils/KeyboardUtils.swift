//
//  KeyboardUtils.swift
//  Domum
//
//  Created by Adheús Rangel on 6/30/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import UIKit


class KeyboardUtils : NSObject {
    
    static var keyboardHeight:CGFloat = 216.0
    static let MARGIN:CGFloat = 20.0
    
    func registerKeyboardHeightObserver() {
        self.initialYView = self.view.frame.origin.y
        NotificationCenter.default.addObserver(self,
                selector: #selector(KeyboardUtils.keyboardShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    func unregisterKeyboardHeightObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardShown(_ notification: Notification) {
        let info  = (notification as NSNotification).userInfo!
        let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]! as AnyObject
        
        let rawFrame = value.cgRectValue
        let keyboardFrame = view.convert(rawFrame!, from: nil)
        
        KeyboardUtils.keyboardHeight = keyboardFrame.height
        
        let textFieldHeight = (self.view.frame.height - (currentTextField.frame.origin.y + currentTextField.frame.height + KeyboardUtils.MARGIN))
        
        if(KeyboardUtils.keyboardHeight > textFieldHeight) {
            self.yViewMoved = textFieldHeight - KeyboardUtils.keyboardHeight
            moveViewHeight(self.initialYView + self.yViewMoved)
        }
    }
    
    var view:UIView! = nil
    var textFields:[UITextField]! = nil
    var yViewMoved:CGFloat = 0.0
    var initialYView:CGFloat = 0.0
    var registeredObserver = false
    var currentTextField:UITextField! = nil
    
    init(parentView:UIView, textFields:[UITextField]) {
        super.init()
        self.view = parentView
        self.initialYView = self.view.frame.origin.y
        self.textFields = textFields
        
        tapOnView()
        registerTargets()
        
    }
    
    func tapOnView() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(KeyboardUtils.tappedOnView))
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    func tappedOnView() {
        for textField in self.textFields {
            textField.resignFirstResponder()
        }
        moveViewHeight(self.initialYView)
        self.yViewMoved = 0
    }
    
    func registerTargets() {
        for textField in self.textFields {
            textField.addTarget(self, action: #selector(KeyboardUtils.editingDidBegin(_:)), for: UIControlEvents.editingDidBegin)
            textField.addTarget(self, action: #selector(KeyboardUtils.goNext(_:)), for: UIControlEvents.editingDidEndOnExit)
        }
    }
    
    func goNext(_ sender:UITextField) {
        var count = 0
        for textField in self.textFields {
            if(textField.isEqual(sender)) {
                break
            }
            count += 1
        }
        if((count+1) >= self.textFields.count) {
            hideKeyboard(sender)
        } else {
            self.textFields[(count+1)].becomeFirstResponder()
            currentTextField = self.textFields[(count+1)]
            let textFieldHeight = (self.view.frame.height - (currentTextField.frame.origin.y + currentTextField.frame.height + KeyboardUtils.MARGIN))
            if(KeyboardUtils.keyboardHeight > textFieldHeight) {
                self.yViewMoved = textFieldHeight - KeyboardUtils.keyboardHeight
                moveViewHeight(self.initialYView + self.yViewMoved)
            }
        }
    }
    
    func editingDidBegin(_ sender:UITextField) {
        self.currentTextField = sender
    }
    
    func hideKeyboard(_ sender:UITextField) {
        if(yViewMoved != 0) {
            moveViewHeight(self.initialYView)
            self.yViewMoved = 0
        }
        sender.resignFirstResponder()
        
    }
    
    
    func moveViewHeight(_ height:CGFloat) {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: height, width: self.view.frame.width, height: self.view.frame.height)
            })
    }
    
    class func moveViewFromTextField(_ view:UIView, currentTextField:UITextField) {
        let textFieldHeight = (view.frame.height - (currentTextField.frame.origin.y + currentTextField.frame.height + KeyboardUtils.MARGIN))
        if(KeyboardUtils.keyboardHeight > textFieldHeight) {
            let yViewMoved = textFieldHeight - KeyboardUtils.keyboardHeight
            UIView.animate(withDuration: 0.5, animations: {
                view.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y + yViewMoved, width: view.frame.width, height: view.frame.height)
            })
        }
    }
    
    class func moveViewToZeroY(_ view:UIView) {
        UIView.animate(withDuration: 0.5, animations: {
            view.frame = CGRect(x: view.frame.origin.x, y: 0, width: view.frame.width, height: view.frame.height)
        })
    }
    
    
}
