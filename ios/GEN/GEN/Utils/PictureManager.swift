//
//  PictureManager.swift
//  vMCard
//
//  Created by Adheús Rangel on 2/18/16.
//  Copyright © 2016 Infinity Labs. All rights reserved.
//

import UIKit

class PictureManager: NSObject {
    
    static let EXTENSION = ".png"
    
    class func save(_ image:UIImage) -> String {
        let pngData = UIImagePNGRepresentation(image)
        let documentPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString
        let filePath = documentPath.appendingPathComponent(UUID().uuidString + EXTENSION)
        try? pngData?.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        return filePath
    }
    
    class func load(_ filePath:String!) -> UIImage? {
        if let pngData = try? Data(contentsOf: URL(fileURLWithPath: filePath)) {
            return UIImage(data: pngData)
        }
        return nil
    }
}
