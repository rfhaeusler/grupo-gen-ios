//
//  PortraitNavigationController.swift
//  Domum
//
//  Created by Adheús Rangel on 9/14/15.
//  Copyright (c) 2015 Volume Creative. All rights reserved.
//

import Foundation
import UIKit

class PortraitNavigationController : UINavigationController {
    
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
}
