//
//  AlertHelper.swift
//  vMCard
//
//  Created by Adheús Rangel on 2/19/16.
//  Copyright © 2016 Infinity Labs. All rights reserved.
//

import UIKit

class AlertHelper: NSObject {
    /*class func showAlertFrom(viewController:UIViewController, title:String! = nil, message:String) -> UIAlertController {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default) { _ in
            // Put here any code that you would like to execute when
            // the user taps that OK button (may be empty in your case if that's just
            // an informative alert)
        }
        alert.addAction(action)
        viewController.presentViewController(alert, animated: true){}
        return alert
    }*/
    /*
    class func showAlertFrom(viewController:UIViewController, title:String! = nil, message:String, withAction:UIAlertAction? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .Alert)
        var cancelActionTitle = "OK"
        var style = UIAlertActionStyle.Cancel
        if let withAction = withAction {
            cancelActionTitle =  "Cancelar"
            style = UIAlertActionStyle.Default
            alert.addAction(withAction)
        }
        let cancelAction = UIAlertAction(title: cancelActionTitle, style: style) { _ in
            // Put here any code that you would like to execute when
            // the user taps that OK button (may be empty in your case if that's just
            // an informative alert)
        }
        alert.addAction(cancelAction)
        viewController.presentViewController(alert, animated: true){}
        return alert
    }
    */
    class func showAlertFrom(_ viewController:UIViewController, title:String! = nil, message:String, withAction:UIAlertAction? = nil, onDismiss:(() -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        var cancelActionTitle = "OK"
        var style = UIAlertActionStyle.cancel
        if let withAction = withAction {
            cancelActionTitle =  "Cancelar"
            style = UIAlertActionStyle.default
            alert.addAction(withAction)
        }
        let cancelAction = UIAlertAction(title: cancelActionTitle, style: style) { _ in
            // Put here any code that you would like to execute when
            // the user taps that OK button (may be empty in your case if that's just
            // an informative alert)
            if let onDismiss = onDismiss {
                onDismiss()
            }
        }
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true){}
        return alert
    }

}
