//
//  ImageDownloader.swift
//  Unimed On Air
//
//  Created by Adheús Rangel on 1/29/16.
//  Copyright © 2016 Infinity Labs. All rights reserved.
//

import UIKit
import AlamofireImage

class ImageDownloader: NSObject {
    
    class func downloadImageFrom(_ url: String, toImageView: UIImageView) {
        if url.isEmpty {
            return
        }
        toImageView.af_setImage(withURL: URL(string: url)!, imageTransition: .crossDissolve(0.3))
//        toImageView.kf.setImage(with: URL(string:url)!,
//                              placeholder: nil,
//                              options: [.transition(.fade(0.3))],
//                              progressBlock: nil,
//                              completionHandler: nil)
    }

}
