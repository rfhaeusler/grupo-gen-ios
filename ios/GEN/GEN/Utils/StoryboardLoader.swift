//
//  StoryboardLoader.swift
//  vMCard
//
//  Created by Adheús Rangel on 2/15/16.
//  Copyright © 2016 Infinity Labs. All rights reserved.
//

import UIKit

class StoryboardLoader: NSObject {
    static func loadViewController(_ viewControllerName:String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: viewControllerName) 
    }
}
