//
//  AreaOptionTableViewCell.swift
//  GEN
//
//  Created by Adheús Rangel on 9/1/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

class AreaOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var areaButton: UIButton!
    
    var area: Area!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(area :Area, selected: Bool = false) {
        self.area = area
        self.areaButton.setTitle(area.title, for: UIControlState())
        self.areaButton.setImage(EffectsHelper.paintImage(self.areaButton.image(for: .selected)!, withColor:
            self.areaButton.titleColor(for: .selected)!), for: .selected)
        self.areaButton.isSelected = selected
    }
    
    @IBAction func onSelected() {
        
        self.areaButton.isSelected = !self.areaButton.isSelected
        if (self.areaButton.isSelected) {
            AppCache.saveArea(area: area)
        } else {
            AppCache.removeArea(area: area)
        }
        
    }

}
