//
//  CategoryListViewController.swift
//  GEN
//
//  Created by Adheús Rangel on 8/24/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

class CategoryListViewController: UITableViewController {

    var area: Area!
    
    var categories = [Category]()
    var bookCount = [BookCount]()
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = area.title
        self.navigationController?.navigationBar.barTintColor = area.color
        
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "SearchIcon"), style: .plain, target: self, action: #selector(CategoryListViewController.searchBarButtonPressed))
        self.navigationItem.rightBarButtonItem = searchBarButton
        
        GENServices.listCategories(self.area.id) { (categoriesOption) in
            if let categories = categoriesOption {
                self.categories = categories
                for category in self.categories {
                    GENServices.categoriesCount(self.area.id, categoryID: category.id, callback: { (count) in
                        var cnt = 0
                        if count != nil {
                            cnt = count!.total
                        }
                        category.bookCount = cnt
                        let idx = self.categories.index(where: { (c) -> Bool in
                            return c.id == category.id
                        })
                        if idx != nil {
                            self.categories.remove(at: idx!)
                            self.categories.insert(category, at: idx!)
                            self.tableView.reloadData()
                        }
                    })
                }
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Auxiliar Functions
    
    func searchBarButtonPressed() {
        let searchViewController = StoryboardLoader.loadViewController("SearchViewController") as! SearchViewController
        AppDelegate.navigateToViewController(searchViewController)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Datasource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CategoryTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! CategoryTableViewCell
        
        let currentItem = self.categories[indexPath.row]
        
        cell.titleLabel.text = currentItem.title
        cell.countLabel.text = "\(currentItem.bookCount)"
        cell.countLabel.textColor = self.area.color
        
        return cell
    }
    
    // MARK: - Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let bookCollectionViewController = StoryboardLoader.loadViewController("BookCollectionViewController") as! BookCollectionViewController
        bookCollectionViewController.area = self.area
        bookCollectionViewController.category = self.categories[indexPath.row]
        
        self.navigationController?.pushViewController(bookCollectionViewController, animated: true)
    }
}
