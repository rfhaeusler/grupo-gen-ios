//
//  MenuViewController.swift
//  Jump Park
//
//  Created by Adheús Rangel on 6/8/16.
//  Copyright © 2016 jump. All rights reserved.
//


import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView:UITableView!
    
    var menuItems = [
        [
            ["title":"CONFIGURAÇÃO", "icon":"SettingsIcon"],
            ["title":"COMO FUNCIONA", "icon":"HowWorksIcon"],
            ["title":"SOBRE", "icon":"AboutIcon"],
            ["title":"FALE CONOSCO", "icon":"FeedbackIcon"],
        ],
        [
        ]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.configureView()
        
    }
    
    func configureView() {
        
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.configureView()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK - UITableview delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems[section].count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch((indexPath as NSIndexPath).section) {
        case 0:
            switch((indexPath as NSIndexPath).row) {
            case 0:
                if !(AppDelegate.currentMainViewController() is SettingsViewController) {
                    let viewController = StoryboardLoader.loadViewController("SettingsViewController")
                    AppDelegate.navigateToViewController(viewController)
                }
                break
            case 1:
                if !(AppDelegate.currentMainViewController() is HowItWorksViewController) {
                    let viewController = StoryboardLoader.loadViewController("HowItWorksViewController")
                    AppDelegate.navigateToViewController(viewController)
                }
                break
            case 2:
                if !(AppDelegate.currentMainViewController() is AboutViewController) {
                    let viewController = StoryboardLoader.loadViewController("AboutViewController")
                    AppDelegate.navigateToViewController(viewController)
                }
                break
            case 3:
                if !(AppDelegate.currentMainViewController() is ContactViewController) {
                    let viewController = StoryboardLoader.loadViewController("ContactViewController")
                    AppDelegate.navigateToViewController(viewController)
                }

                break
            case 4:
                break
            default:
                break
            }
            break
        default:
            switch((indexPath as NSIndexPath).row) {
            case 0:
               
                break
            case 1:
                
                
                break
            case 2:
               
                break
            default:
                break
            }
            break
        }
        AppDelegate.setSideMenuState(SideMenuState.closed, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let myCell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTableViewCell", for: indexPath) as! MenuItemTableViewCell
        
        let currentItem = menuItems[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row]
        
        myCell.setTitle(currentItem["title"]!, icon: currentItem["icon"]!)
        
        return myCell
    }
    
}
