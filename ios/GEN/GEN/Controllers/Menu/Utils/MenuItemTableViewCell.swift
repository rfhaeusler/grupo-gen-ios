//
//  MenuItemTableViewCell.swift
//  Jump Park
//
//  Created by Adheús Rangel on 6/8/16.
//  Copyright © 2016 jump. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var title:UILabel!
    

    
    var buttonAction:(() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setTitle(_ title:String, icon:String, showButton:Bool = false, buttonAction:(() -> Void)? = nil) {
        self.title.text = title
        self.icon.image = UIImage(named: icon)!
        //self.title.textColor = ColorUtils.getSubAccentColor()
        //EffectsHelper.loadImage(icon, toImageView: self.icon, withColor: ColorUtils.getSubAccentColor())
        
    }
}
