//
//  AreaButton.swift
//  GEN
//
//  Created by Luiz Aires Soares on 13/06/17.
//  Copyright © 2017 Jump Tecnologia. All rights reserved.
//

import UIKit
import AlamofireImage

class AreaButton: UIView {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var btn: UIButton!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> AreaButton {
        return UINib(nibName: "AreaButton", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AreaButton
    }
    
    func configAreaButton(iconName: String?, name: String, backgroundColor: UIColor, size: CGSize) {
        self.frame = CGRect(x: 0, y: 0,width: size.width, height: size.height)
        self.backgroundColor = backgroundColor
        
        if let icon = iconName {
            self.img.af_setImage(withURL: URL(string: icon)!,
                                 filter: AspectScaledToFitSizeFilter(size: CGSize(width: 60, height: 60)),
                                completion: { result in
                                    if let image = result.result.value {
                                        self.img.image = image
                                    }
            })
        }
        self.lbl.text = name
        
        EffectsHelper.applyShadow(self)
    }

}
