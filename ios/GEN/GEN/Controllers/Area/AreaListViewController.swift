//
//  CategoryListViewController.swift
//  GEN
//
//  Created by Adheús Rangel on 8/24/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

class AreaListViewController: UIViewController {
    
    var areasCache = [Area]()
        
    var areas = [Area]()
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UIApplication.shared.isStatusBarHidden = false
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: .plain, target: self, action: #selector(AreaListViewController.openMenu))
        self.navigationItem.leftBarButtonItem = menuButton
        
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "SearchIcon"), style: .plain, target: self, action: #selector(AreaListViewController.searchBarButtonPressed))
        self.navigationItem.rightBarButtonItem = searchBarButton
        
        GENServices.listAreas { (areasOption) in
            if let areas = areasOption {
                self.areasCache = areas
                self.areas = self.areasCache.filter { AppCache.getAreas().contains($0.id) }
                self.generateAreasButtons()
                self.requestNotifications()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = ColorUtils.getAccentColor()
        
        if AppCache.isFirstRun() {
            let tutorialViewController = StoryboardLoader.loadViewController("TutorialPagesViewController")
            AppDelegate.navigateToViewController(tutorialViewController)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.areas = areasCache.filter { AppCache.getAreas().contains($0.id) }
        self.generateAreasButtons()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        self.generateAreasButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Auxiliar Functions
    
    func openMenu() {
        AppDelegate.setSideMenuState(SideMenuState.open, animated: true)
    }
    
    func searchBarButtonPressed() {
        let searchViewController = StoryboardLoader.loadViewController("SearchViewController") as! SearchViewController
        AppDelegate.navigateToViewController(searchViewController)
    }
    
    func generateAreasButtons() {
        for subview in self.view.subviews {
            subview.removeFromSuperview()
        }
        let size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height/CGFloat(self.areas.count))
        
        var count: CGFloat = 0
        for area in areas {
            let areaButton = makeAreaButton(area.iconURL, name: area.title, backgroundColor: area.color, size: size)
            areaButton.frame = CGRect(x: 0, y: (count * areaButton.frame.size.height), width: areaButton.frame.size.width, height: areaButton.frame.size.height)
            
            areaButton.btn.addTarget(self, action: #selector(AreaListViewController.onSelectedAreaButton), for: .touchUpInside)
            areaButton.btn.tag = Int(count)
            
            self.view.addSubview(areaButton)
            count += 1
        }
    }
    
    func makeAreaButton(_ iconName: String?, name: String, backgroundColor: UIColor, size: CGSize) -> AreaButton {
        let view = AreaButton.instanceFromNib()
        view.frame = CGRect(x: 0, y: 0,width: size.width, height: size.height)
        view.configAreaButton(iconName: iconName, name: name, backgroundColor: backgroundColor, size: size)
        
        return view
    }
    
    func onSelectedAreaButton(_ sender: UIButton) {
        let area = self.areas[sender.tag]
        
        let categoryListViewController = StoryboardLoader.loadViewController("CategoryListViewController") as! CategoryListViewController
        categoryListViewController.area = area
        
        self.navigationController?.pushViewController(categoryListViewController, animated: true)
    }
    
    func requestNotifications() {
        GENServices.notifications { (notifications) in
            DispatchQueue.main.async {
                if let arr = notifications {
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        var savedNotifications = UserDefaults.standard.array(forKey: "saved_notifications") as? [Int]
                        for notification in arr {
                            if savedNotifications == nil || savedNotifications?.contains(notification.id) == false {
                                appDelegate.createAndShowNotification(notification: notification)
                                if savedNotifications == nil {
                                    savedNotifications = [Int]()
                                }
                                savedNotifications?.append(notification.id)
                            }
                        }
                        if savedNotifications != nil {
                            UserDefaults.standard.set(savedNotifications, forKey: "saved_notifications")
                        }
                    }
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
