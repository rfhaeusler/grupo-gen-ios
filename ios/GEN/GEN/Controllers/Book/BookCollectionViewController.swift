//
//  BookCollectionViewController.swift
//  GEN
//
//  Created by Adheús Rangel on 8/25/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

private let reuseIdentifier = "BookCollectionCell"

class BookCollectionViewController: UICollectionViewController {
    
    var area: Area!
    var category: Category!
        
    var books = [Book]()
    var newestBooks = [Book]()
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = self.category.title
        
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "SearchIcon"), style: .plain, target: self, action: #selector(BookCollectionViewController.searchBarButtonPressed))
        self.navigationItem.rightBarButtonItem = searchBarButton
        
        GENServices.listBooks(self.area.id, categoryID: self.category.id) { (booksOption) in
            if let books = booksOption {
                self.newestBooks = books.filter { $0.isRelease == true }
                self.books = books.filter { $0.isRelease == false }
                if self.newestBooks.count == 0 {
                    if self.books.count > 0 {
                        if self.books.count > 1 && self.books.count <= 3 {
                            self.newestBooks.append(self.books[0])
                            self.books.remove(at: 0)
                        } else if self.books.count > 3 {
                            var idx = 0
                            while idx < 3 {
                                self.newestBooks.append(self.books[0])
                                self.books.remove(at: 0)
                                idx += 1
                            }
                        }
                    }
                }
                self.collectionView?.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Auxiliar Functions
    
    func searchBarButtonPressed() {
        let searchViewController = StoryboardLoader.loadViewController("SearchViewController") as! SearchViewController
        AppDelegate.navigateToViewController(searchViewController)
    }
    
    func goToBookDetails(_ book: Book) {
        let bookDetailsViewController = StoryboardLoader.loadViewController("BookDetailsViewController") as! BookDetailsViewController
        bookDetailsViewController.accentColor = self.area.color
        bookDetailsViewController.book = book
        
        self.navigationController?.pushViewController(bookDetailsViewController, animated: true)
    }
    
    @IBAction func downloadButtonTouched(sender: UIButton) {
        let selected: Book = self.books[sender.tag]
        if let preview = selected.previewPDFURL, preview != " " {
            UIApplication.shared.openURL(URL(string: preview)!)
        } else {
            let alert = UIAlertController(title: nil, message: "Livro não possui PDF", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        var num: CGFloat = CGFloat(self.books.count)/CGFloat(2)
        num = num.rounded(.up)
        return Int(num)
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var num: CGFloat = CGFloat(self.books.count)/CGFloat(2)
        num = num.rounded(.up)
        if (self.books.count % 2 != 0 && (section+1) == Int(num)) {
            return 1
        }
        return 2
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BookCollectionCell
    
        // Configure the cell
        let idx = (indexPath.section*2)+indexPath.row
        if (idx < self.books.count) {
            cell.configureCell(self.books[idx])
            cell.downloadButton.addTarget(self, action: #selector(downloadButtonTouched(sender:)), for: .touchUpInside)
            cell.downloadButton.tag = idx
        }
        return cell
    }

    // MARK: - UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let idx = (indexPath.section*2)+indexPath.row
        
        self.goToBookDetails(self.books[idx])
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        let header =  collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                      withReuseIdentifier: "BookCollectionHeaderView",
                                                                      for: indexPath) as! BookCollectionHeaderView
        header.books = self.newestBooks
        header.configureView(self.area.color)
        header.onSelectedBook = { (book) in
            self.goToBookDetails(book)
        }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize(width: collectionView.frame.size.width, height: 318)
        }
        return .zero
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }

    */
}

extension BookCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var num: CGFloat = CGFloat(self.books.count)/CGFloat(2)
        num = num.rounded(.up)
        if (self.books.count % 2 != 0 && (section+1) == Int(num)) {
            let cellSize: CGFloat = 168.0
            let inset = (collectionView.frame.size.width-cellSize)/2
            
            return UIEdgeInsetsMake(0, inset, 0, inset)
        } else {
            return UIEdgeInsetsMake(0, 8, 0, 8)
        }
    }
}
