//
//  BookCollectionHeaderView.swift
//  GEN
//
//  Created by Adheús Rangel on 8/25/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

class BookCollectionHeaderView: UICollectionReusableView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var books = [Book]()
    
    @IBOutlet weak var sectionView: UIView!
    
    @IBOutlet weak var newBooksLabel: UILabel!
    @IBOutlet weak var otherBooksLabel: UILabel!
    
    @IBOutlet weak var newBooksCollection: UICollectionView!

    var accentColor: UIColor!
    
    var onSelectedBook: ((Book) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureView(_ accentColor: UIColor) {
        self.accentColor = accentColor
        
        self.sectionView.backgroundColor = self.accentColor
        
        EffectsHelper.applyBorders(self.newBooksLabel)
        self.otherBooksLabel.textColor = self.accentColor
        self.otherBooksLabel.tintColor = self.accentColor
        EffectsHelper.applyBorders(self.otherBooksLabel)
        
        self.newBooksCollection.dataSource = self
        self.newBooksCollection.delegate = self
        self.newBooksCollection.reloadData()
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.books.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCollectionCell", for: indexPath) as! BookCollectionCell
        
        // Configure the cell
        cell.configureCell(self.books[indexPath.row], darkMode: true)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if let onSelectedBook = self.onSelectedBook {
            onSelectedBook(self.books[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let cellSize: CGFloat = 168.0
        let inset = (collectionView.frame.size.width-cellSize)/2
        
        return UIEdgeInsetsMake(0, inset, 0, inset)
    }
}
