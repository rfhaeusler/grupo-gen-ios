//
//  BookCollectionCellCollectionViewCell.swift
//  GEN
//
//  Created by Adheús Rangel on 8/25/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import AlamofireImage

class BookCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var coverPicture: UIImageView!
    
    @IBOutlet weak var actionBarView:UIView!
    
    @IBOutlet weak var downloadButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ book: Book, darkMode: Bool = false) {   
        if !darkMode {
            self.downloadButton.setImage(EffectsHelper.paintImage(self.downloadButton.image(for: UIControlState())!, withColor: UIColor.lightGray), for: UIControlState())
        } else {
            self.downloadButton.isHidden = true
        }
        self.coverPicture.af_setImage(withURL: URL(string: book.coverPictureURL)!,
                                      placeholderImage:UIImage(named: "CoverExample")) { (result) in
            if let image = result.result.value {
                self.coverPicture.image = image
            } else {
                self.coverPicture.image = UIImage(named: "CoverExample")
            }
        }
        self.actionBarView.isHidden = "true" == "false"
    }
}
