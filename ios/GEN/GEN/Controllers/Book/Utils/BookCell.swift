//
//  BookCell.swift
//  GEN
//
//  Created by Adheús Rangel on 8/25/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {
    
    @IBOutlet weak var coverPicture:UIImageView!
    
    @IBOutlet weak var actionBarView:UIView!
    
    @IBOutlet weak var viewButton:UIButton!
    @IBOutlet weak var removeButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
