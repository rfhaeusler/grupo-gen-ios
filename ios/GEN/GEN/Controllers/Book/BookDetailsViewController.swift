//
//  BookDetailsViewController.swift
//  GEN
//
//  Created by Adheús Rangel on 8/26/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import AlamofireImage

class BookDetailsViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var coverPicture: UIImageView!
    
    @IBOutlet weak var authorsLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var editionLabel: UILabel!
    
    @IBOutlet weak var numberOfPagesLabel: UILabel!
    
    @IBOutlet weak var formatLabel: UILabel!
    
    @IBOutlet weak var finalizationLabel: UILabel!
    
    @IBOutlet weak var editorLabel: UILabel!
    
    @IBOutlet weak var isbnLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var isSuplementarMaterial: UILabel!
    
    @IBOutlet weak var btnDownloadSample: UIButton!
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var tabBar: UITabBar!
    
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    var accentColor: UIColor!
    
    var book: Book!
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "SearchIcon"), style: .plain, target: self, action: #selector(BookDetailsViewController.searchBarButtonPressed))
        self.navigationItem.rightBarButtonItem = searchBarButton
        
        let colorfulLabels = [self.authorsLabel,
                              self.editionLabel,
                              self.numberOfPagesLabel,
                              self.formatLabel,
                              self.finalizationLabel,
                              self.editorLabel,
                              self.isbnLabel,
                              self.weightLabel,
                              self.isSuplementarMaterial]
        
        for label in colorfulLabels {
            label?.textColor = self.accentColor
        }
        
//        self.tabBar.tintColor = self.accentColor
        
        if self.book != nil {
            self.configureBook()
        }
        
        self.webView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Auxiliar Functions
    
    func configureBook() {
        self.title =  self.book.title
        self.titleLabel.text = self.book.title
        self.coverPicture.af_setImage(withURL: URL(string: self.book.coverPictureURL)!, placeholderImage: UIImage(named: "CoverExample"))
        self.authorsLabel.text = self.book.author
        self.editionLabel.text = self.book.edition
        self.numberOfPagesLabel.text = String(self.book.pageNumber)
        self.formatLabel.text = self.book.format
        self.finalizationLabel.text = self.book.finalization
        self.editorLabel.text =  self.book.editor
        self.isbnLabel.text =  self.book.isbn
        self.weightLabel.text = String(format: "%.02f kg", self.book.weight)
        self.isSuplementarMaterial.text = self.book.isSuplementar == true ? "Sim" : "Não"
        self.btnDownloadSample.tintColor = self.accentColor ?? UIColor.black
        self.btnDownloadSample.setTitleColor(self.accentColor ?? UIColor.black, for: .normal)
        self.webView.loadHTMLString(self.book.summary, baseURL: nil)
    }
    
    func searchBarButtonPressed() {
        let searchViewController = StoryboardLoader.loadViewController("SearchViewController") as! SearchViewController
        AppDelegate.navigateToViewController(searchViewController)
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func btnDownloadSampleTouched(_ sender: UIButton) {
        if let preview = self.book.previewPDFURL, preview != " " {
            UIApplication.shared.openURL(URL(string: preview)!)
        } else {
            self.showAlert(message: "Livro não possui amostra")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension BookDetailsViewController: UITabBarDelegate {

    // MARK: - Tabbar Delegate
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        tabBar.selectedItem = nil
        switch item.tag {
        case 0:
            if let preview = self.book.previewPDFURL, preview != " " {
                UIApplication.shared.openURL(URL(string: preview)!)
            } else {
                self.showAlert(message: "Livro não possui PDF")
            }
            break
        case 1:
            UIApplication.shared.openURL(URL(string: "http://www.grupogen.com.br/relacionamento-professores/")!)
            break
        case 2:
            if let link = self.book.storeLink, link != " " {
                UIApplication.shared.openURL(URL(string: link)!)
            } else {
                self.showAlert(message: "Livro não possui link para a loja")
            }
            break
        case 3:
            if let link = self.book.coverPictureURL, link != " " {
                let str = "Gostei desse livro e recomendo para você! Link para o livro na nossa loja: \(link). Aproveite!"
                let activityController = UIActivityViewController(activityItems: [str, self.coverPicture.image!], applicationActivities: nil)
                activityController.excludedActivityTypes = [.addToReadingList,
                                                            .airDrop,
                                                            .assignToContact,
                                                            .copyToPasteboard,
                                                            .openInIBooks,
                                                            .postToFlickr,
                                                            .postToVimeo,
                                                            .postToWeibo,
                                                            .postToTencentWeibo,
                                                            .print,
                                                            .saveToCameraRoll]
                self.present(activityController, animated: true, completion: nil)
            } else {
                self.showAlert(message: "Livro não possui link!")
            }
            break
        default:
            break
        }
    }
}

extension BookDetailsViewController: UIWebViewDelegate {
    
    // MARK: - WebViewDelegate
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily =\"-apple-system\"")
        UIView.animate(withDuration: 0.1) {
            self.tableBottomConstraint.constant = webView.scrollView.contentSize.height
            self.tblView.contentSize = CGSize(width: self.tblView.contentSize.width,
                                              height: (self.tblView.contentSize.height + webView.scrollView.contentSize.height))
        }
    }
    
}
