//
//  SearchViewController.swift
//  GEN
//
//  Created by Adheús Rangel on 9/19/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var tableView:UITableView!
    
    var results = [Book]()
    var searchedTerm: String?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchBar.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SearchTableViewCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! SearchTableViewCell
        
        cell.configure(self.results[(indexPath as NSIndexPath).row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let bookDetailsViewController = StoryboardLoader.loadViewController("BookDetailsViewController") as! BookDetailsViewController
        bookDetailsViewController.accentColor =  self.navigationController?.navigationBar.barTintColor
        bookDetailsViewController.book = self.results[(indexPath as NSIndexPath).row]
        self.navigationController?.pushViewController(bookDetailsViewController, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let terms = searchBar.text , terms.characters.count >= 3 {
            GENServices.searchBooks(terms, callback: { (booksMaybe) in
                if let books = booksMaybe {
                    self.results = books
                    if self.results.count == 0 {
                        self.tableView.isHidden = true
                    } else {
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
                } else {
                    self.tableView.isHidden = true
                }
            })
        }

    }
}
