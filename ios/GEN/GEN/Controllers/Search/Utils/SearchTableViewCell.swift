//
//  SearchTableViewCell.swift
//  GEN
//
//  Created by Adheús Rangel on 9/19/16.
//  Copyright © 2016 Jump Tecnologia. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImageView:UIImageView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var descriptionLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ book: Book) {
        if let url = book.coverPictureURL {
            self.coverImageView.af_setImage(withURL: URL(string: url)!,
                                            placeholderImage: UIImage(named: "CoverExample")!)
        }
        self.titleLabel.text = book.title
        self.descriptionLabel.text = book.author + " - " + book.editor
    }

}
